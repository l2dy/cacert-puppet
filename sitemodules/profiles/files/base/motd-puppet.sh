#!/bin/sh

cat <<EOD
   _________                  __
  / ____/   | ________  _____/ /_
 / /   / /| |/ ___/ _ \/ ___/ __/
/ /___/ ___ / /__/  __/ /  / /_
\____/_/  |_\___/\___/_/   \__/

This system is managed by Puppet. Access to the system is restricted. Passwords
for administrators are managed by Puppet. Please see the Puppet definitions and
the infrastructure documentation for more information.

https://infradocs.cacert.org/
https://git.cacert.org/gitweb/?p=cacert-puppet.git
EOD
