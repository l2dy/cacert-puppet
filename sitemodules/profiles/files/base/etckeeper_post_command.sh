#!/bin/sh
set -e

if etckeeper unclean; then
	if ! etckeeper commit "saving uncommitted changes in /etc after puppet run"; then
		echo "warning: etckeeper failed to commit changes in /etc using $VCS" >&2
	fi
fi
