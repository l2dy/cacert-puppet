#!/bin/sh
### BEGIN INIT INFO
# Provides:          git-pull-hook
# Required-Start:    $remote_fs
# Required-Stop:     $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: CAcert puppet git pull hook
# Description:       CAcert puppet git pull hook
### END INIT INFO

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

BASE=git-pull-hook

GIT_PULL_HOOK=/usr/local/sbin/git-pull-hook
GIT_PULL_HOOK_PIDFILE=/var/run/$BASE.pid
GIT_PULL_HOOK_LOGFILE=/var/log/$BASE.log
GIT_PULL_HOOK_DESC="Puppet git pull hook"

# Get lsb functions
. /lib/lsb/init-functions

# Check git-pull-hook is present
if [ ! -x $GIT_PULL_HOOK ]; then
  log_failure_msg "$GIT_PULL_HOOK not present or not executable"
  exit 1
fi

fail_unless_root() {
  if [ "$(id -u)" != '0' ]; then
    log_failure_msg "$GIT_PULL_HOOK_DESC must be run as root"
    exit 1
  fi
}

case "$1" in
  start)
    fail_unless_root

    touch "$GIT_PULL_HOOK_LOGFILE"
    chown root:adm "$GIT_PULL_HOOK_LOGFILE"

    log_begin_msg "Starting $GIT_PULL_HOOK_DESC: $GIT_PULL_HOOK"
    start-stop-daemon --start --background --no-close \
      --exec "$GIT_PULL_HOOK" \
      --pidfile "$GIT_PULL_HOOK_PIDFILE" \
      --chdir "/" \
      --make-pidfile \
         >> "$GIT_PULL_HOOK_LOGFILE" 2>&1
    log_end_msg $?
    ;;

  stop)
    fail_unless_root
    if [ -f "$GIT_PULL_HOOK_PIDFILE" ]; then
      start-stop-daemon --stop --pidfile "$GIT_PULL_HOOK_PIDFILE" --retry 5
      log_end_msg $?
    else
      log_warning_msg "$GIT_PULL_HOOK_DESC already stopped - file $GIT_PULL_HOOK_PIDFILE not found."
    fi
    ;;

  restart)
    fail_unless_root
  git_pull_hook_pid=`cat "$GIT_PULL_HOOK_PIDFILE" 2> /dev/null`
  [ -n "$git_pull_hook_pid" ] \
    && ps -p $git_pull_hook_pid > /dev/null 2>&1 \
    && $0 stop
  $0 start
  ;;

  force-reload)
    fail_unless_root
    $0 restart
    ;;

  status)
    status_of_proc -p "$GIT_PULL_HOOK_PIDFILE" "$GIT_PULL_HOOK" "$GIT_PULL_HOOK_DESC"
    ;;

  *)
    echo "Usage: service git-pull-hook {start|stop|restart|force-reload|status}"
    exit 1
    ;;
esac
