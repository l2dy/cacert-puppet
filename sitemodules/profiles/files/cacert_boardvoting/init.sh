#!/bin/sh
### BEGIN INIT INFO
# Provides:          boardvoting
# Required-Start:    $remote_fs
# Required-Stop:     $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: CAcert board voting system
# Description:       CAcert board voting system
### END INIT INFO

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

BASE=boardvoting

BOARDVOTING=/usr/local/bin/cacert-boardvoting
BOARDVOTING_PIDFILE=/var/run/$BASE.pid
BOARDVOTING_LOGFILE=/var/log/$BASE.log
BOARDVOTING_WORKDIR=/var/lib/$BASE
BOARDVOTING_USER=$BASE
BOARDVOTING_OPTS=
BOARDVOTING_DESC="Boardvoting"

# Get lsb functions
. /lib/lsb/init-functions

if [ -f /etc/default/$BASE ]; then
  . /etc/default/$BASE
fi

# Check boardvoting is present
if [ ! -x $BOARDVOTING ]; then
  log_failure_msg "$BOARDVOTING not present or not executable"
  exit 1
fi

fail_unless_root() {
  if [ "$(id -u)" != '0' ]; then
    log_failure_msg "$BOARDVOTING_DESC must be run as root"
    exit 1
  fi
}

case "$1" in
  start)
    fail_unless_root

    touch "$BOARDVOTING_LOGFILE"
    chown boardvoting:adm "$BOARDVOTING_LOGFILE"

    log_begin_msg "Starting $BOARDVOTING_DESC: $BOARDVOTING"
    start-stop-daemon --start --background --no-close \
      --exec "$BOARDVOTING" \
      --user "$BOARDVOTING_USER" \
      --pidfile "$BOARDVOTING_PIDFILE" \
      --chdir "$BOARDVOTING_WORKDIR" \
      --make-pidfile \
      -- \
        $BOARDVOTING_OPTS >> "$BOARDVOTING_LOGFILE" 2>&1
    log_end_msg $?
    ;;

  stop)
    fail_unless_root
    if [ -f "$BOARDVOTING_PIDFILE" ]; then
      start-stop-daemon --stop --pidfile "$BOARDVOTING_PIDFILE" --retry 5
      log_end_msg $?
    else
      log_warning_msg "$BOARDVOTING_DESC already stopped - file $BOARDVOTING_PIDFILE not found."
    fi
    ;;

  restart)
    fail_unless_root
  boardvoting_pid=`cat "$BOARDVOTING_PIDFILE" 2> /dev/null`
  [ -n "$boardvoting_pid" ] \
    && ps -p $boardvoting_pid > /dev/null 2>&1 \
    && $0 stop
  $0 start
  ;;

  force-reload)
    fail_unless_root
    $0 restart
    ;;

  status)
    status_of_proc -p "$BOARDVOTING_PIDFILE" "$BOARDVOTING" "$BOARDVOTING_DESC"
    ;;

  *)
    echo "Usage: service boardvoting {start|stop|restart|force-reload|status}"
    exit 1
    ;;
esac
