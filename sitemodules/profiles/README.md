# profiles

#### Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with profiles](#setup)
    * [Beginning with profiles](#beginning-with-profiles)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Reference](#reference)
1. [Limitations - OS compatibility, etc.](#limitations)

## Description

The profiles module defines functionality that is part of node roles. Profiles
should be assigned to nodes by including them in the node's role class.

## Setup

### Beginning with profiles

To use a profile you should define its required parameters in hiera data and
include the profile in the node's role class.

## Usage

Usage is very specific to the particular profile classes. See the class
documentation for the profiles you want to use.

## Reference

### Classes

#### Public classes

*[`profiles::base`](#profiles-base): Setup the base functionality for a puppet managed node.

## Limitations

This module is designed to be used on CAcert infrastructure only. It is not
designed for reuse anywhere else. The CAcert infrastructure is described at https://infradocs.cacert.org/.
