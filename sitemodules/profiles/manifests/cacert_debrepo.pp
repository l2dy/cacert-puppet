# Class: profiles::cacert_debrepo
# ===============================
#
# Setup for the CAcert internal debian repository.
#
# This manifest is meant to be included from other manifests.
#
# Examples
# --------
#
# @example
#   include profiles::cacert_debrepo
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2019 Jan Dittberner
class profiles::cacert_debrepo () {
  include apt
  apt::key { 'cacert':
    id      => '4C4F8164EFE3DAFEC82F22FC82D61CAA4E904466',
    source  => 'http://webstatic.infra.cacert.org/cacert-debian-archive-2019.gpg',
    options => 'http-proxy=http://proxyout:3128',
  }
  apt::source { 'cacert':
    location => 'http://webstatic.infra.cacert.org',
    repos    => 'main',
    release  => "${::lsbdistcodename}-cacert",
  }
}
