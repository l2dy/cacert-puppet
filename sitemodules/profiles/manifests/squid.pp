# Class: profiles::squid.pp
# =========================
#
# This class defines a Squid proxy installation that allows outgoing http and
# https traffic to selected destinations.
#
# Parameters
# ----------
#
# @param acls a list of squid ACLs for regulating outgoing traffic
#
# Examples
# --------
#
# @example
#   class roles::myhost {
#     include profiles::squid
#   }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2017 Jan Dittberner
class profiles::squid (
  Optional[Array[String]] $acls = undef,
  Optional[Array[String]] $http_access = undef,
) {
  package { 'squid':
    ensure => latest,
  }

  service { 'squid':
    ensure => running,
  }

  file { '/etc/squid/conf.d/cacert-acls.conf':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => epp(
      'profiles/squid/squid.conf.epp',
      {
        'acls'        => $acls,
        'http_access' => $http_access
      }
    ),
    require => Package['squid'],
    notify  => Service['squid'],
  }
}
