# Class: profiles::icinga2_master
# ===============================
#
# This class installs and configures the Icinga2 master with
# PostgreSQL IDO backend
#
# Parameters
# ----------
#
# @param ido_database_password   database password for Icinga2 IDO database
#
# @param web2_database_password  database password for IcingaWeb2 database
#
# @param api_users               Icinga2 API users
#
# @param pki_ticket_salt         Ticket salt for API endpoint
#
# @param ca_key                  Icinga2 CA private key content
#
# @param ca_certificate          Icinga2 CA certificate content
#
# @param $icingaweb_admins       List of icingaweb admin users
#
# @param git_pull_ssh_passphrase passphrase to use for the ssh key to pull new
#                                configuration from the configuration repository
#
# @param git_pull_directory      directory where the icinga2 configuration
#                                is checked out
#
# @param git_pull_tokens         list of tokens that are valid to trigger the
#                                git pull hook
#
# @param git_repository          configuration git repository
#
# @param git_branch              configuration branch in the git repository
#
# Examples
# --------
#
# @example
#   class roles::myhost {
#     include profiles::icinga2_master
#   }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2019 Jan Dittberner
class profiles::icinga2_master (
  String $ido_database_password,
  String $web2_database_password,
  Hash[String, Hash[String, Variant[String, Tuple[String, 1]]]] $api_users,
  String $pki_ticket_salt,
  String $ca_key,
  String $ca_certificate,
  Array[String] $icingaweb_admins = ['icingaadmin'],
  String $git_pull_ssh_passphrase,
  String $git_pull_directory = '/etc/icinga2/conf.d',
  Array[String] $git_pull_tokens,
  String $git_repository = 'icinga2git@git:/var/lib/git/cacert-icinga2-conf_d.git',
  String $git_branch = 'master',
) {
  include profiles::icinga2_common
  include profiles::systemd_reload
  include postgresql::server

  class { '::icinga2':
    manage_repo => false,
    features    => ['mainlog', 'checker', 'notification'],
    constants   => {
      'TicketSalt' => $pki_ticket_salt,
      'ZoneName'   => $::fqdn,
    },
  }

  file { $::icinga2::globals::ca_dir:
    ensure => directory,
    owner  => 'nagios',
    group  => 'nagios',
    mode   => '0755',
  } ->
  class { '::icinga2::pki::ca':
    ca_cert => $ca_certificate,
    ca_key  => $ca_key,
  }

  postgresql::server::db { 'icinga2':
    user     => 'icinga2',
    password => postgresql_password('icinga2', $ido_database_password),
  }

  class { '::icinga2::feature::idopgsql':
    user          => 'icinga2',
    password      => $ido_database_password,
    database      => 'icinga2',
    import_schema => true,
    require       => Postgresql::Server::Db['icinga2'],
  }

  class { '::icinga2::feature::api':
    pki        => 'none',
  }

  icinga2::object::zone { 'global-templates':
    global => true,
  }

  create_resources(icinga2::object::apiuser, $api_users)

  Icinga2::Object::Zone <<| |>> ~> Service['icinga2']
  Icinga2::Object::Endpoint <<| |>> ~> Service['icinga2']

  package { [
    'apache2', 'libapache2-mod-php', 'php-pgsql', 'php', 'php-cli', 'php-curl',
    'php-gd', 'php-ldap', 'php-json', 'php-intl', 'php-imagick']:
      ensure => latest,
  }

  postgresql::server::db { 'icingaweb2':
    user     => 'icingaweb2',
    password => postgresql_password(
      'icingaweb2', $web2_database_password
    ),
  }

  class { '::icingaweb2':
    manage_repo   => false,
    import_schema => true,
    db_type       => 'pgsql',
    db_host       => 'localhost',
    db_port       => 5432,
    db_username   => 'icingaweb2',
    db_password   => $web2_database_password,
    require       => Postgresql::Server::Db['icingaweb2'],
  }

  class { '::icingaweb2::module::monitoring':
    ido_type          => 'pgsql',
    ido_host          => 'localhost',
    ido_port          => 5432,
    ido_db_name       => 'icinga2',
    ido_db_username   => 'icinga2',
    ido_db_password   => $ido_database_password,
    commandtransports => {
      icinga2 => {
        transport => 'api',
        username  => 'root',
        password  => $api_users['root']['password'],
      }
    }
  }

  icingaweb2::config::authmethod { 'external-authentication':
    backend => 'external',
    require => Class['::icingaweb2'],
  }

  icingaweb2::config::role { 'admin':
    users       => join($icingaweb_admins, ","),
    permissions => '*',
    require     => Class['::icingaweb2'],
  }

  package { ['sshpass', 'git']:
    ensure => installed,
  }

  $git_pull_hook = '/usr/local/sbin/icinga2-git-pull-hook'
  $git_pull_hook_config = '/etc/default/icinga2-git-pull-hook.ini'
  $git_pull_hook_service = '/etc/systemd/system/icinga2-git-pull-hook.service'

  file { $git_pull_hook:
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0750',
    source  => 'puppet:///modules/profiles/icinga2_master/icinga2-git-pull-hook',
    require => [Package['sshpass'], Package['git']],
    notify => Exec['reload systemd configuration'],
  }

  file { $git_pull_hook_service:
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/profiles/icinga2_master/icinga2-git-pull-hook.service',
    notify => Exec['reload systemd configuration'],
  }

  file { $git_pull_hook_config:
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0400',
    content => epp(
      'profiles/icinga2_master/icinga2-git-pull-hook.ini.epp',
      {
        'ssh_passphrase' => $git_pull_ssh_passphrase,
        'tokens'         => $git_pull_tokens,
        'git_directory'  => $git_pull_directory,
        'git_repository' => $git_repository,
        'git_branch'     => $git_branch,
      }
    ),
    notify => Exec['reload systemd configuration'],
  }

  service { 'icinga2-git-pull-hook':
    ensure  => running,
    enable  => true,
    require => [
      File[$git_pull_hook],
      File[$git_pull_hook_config],
      File[$git_pull_hook_service],
    ],
  }
}
