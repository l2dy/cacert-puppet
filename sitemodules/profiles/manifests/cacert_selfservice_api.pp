# Class: profiles::cacert_selfservice_api
# =======================================
#
# This class defines the cacert_selfservice_api profile that installs the
# CAcert community self service system API backend
#
# Parameters
# ----------
#
# @param server_certificate             PEM encoded X.509 server certificate
#
# @param server_private_key             PEM encoded unencrypted RSA private key
#
# @param db_username                    MariaDB/MySQL user name
#
# @param db_password                    MariaDB/MySQL password
#
# @param db_name                        MariaDB/MySQL database name
#
# @param notification_recipient_address notification email recipient address
#
# @param notification_recipient_name    notification email recipient name
#
# @param notification_sender_address    notification email sender address
#
# @param mail_host                      hostname or IP address of the outgoing
#                                       email server
#
# @param mail_port                      TCP port number of the outgoing email
#                                       server
#
# @param client_identities              List of client identies consisting of an
#                                       id and key field for each client
#
# Examples
# --------
#
# @example
#   class roles::myhost {
#     include profiles::cacert_selfservice_api
#   }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2019 Jan Dittberner
#
class profiles::cacert_selfservice_api (
  String $server_certificate,
  String $server_private_key,
  String $listen_address = ":9443",
  String $db_username,
  String $db_password,
  String $db_name = 'cacertusers',
  String $notification_recipient_address = 'email-admin@cacert.org',
  String $notification_recipient_name = 'CAcert email administrators',
  String $notification_sender_address = 'returns@cacert.org',
  String $mail_host = 'localhost',
  Integer $mail_port = 25,
  Array[Hash[String, String]] $client_identities,
) {
  include profiles::cacert_debrepo

  $service_name = 'cacert-selfservice-api'
  $config_directory = "/etc/${service_name}"
  $config_file = "${config_directory}/config.yaml"
  $server_certificate_file = "${config_directory}/certs/server.crt.pem"
  $server_key_file = "${config_directory}/private/server.key.pem"
  $log_directory = "/var/log/${service_name}"

  package { $service_name:
    ensure  => latest,
    require => Apt::Source['cacert'],
  }

  file { $log_directory:
    ensure  => directory,
    owner   => $service_name,
    group   => 'root',
    mode    => '0750',
    require => Package[$service_name],
  }
  file { "${config_directory}/certs":
    ensure  => directory,
    owner   => $service_name,
    group   => 'root',
    mode    => '0750',
    require => Package[$service_name],
  }
  file { "${config_directory}/private":
    ensure  => directory,
    owner   => $service_name,
    group   => 'root',
    mode    => '0700',
    require => Package[$service_name],
  }
  file { $server_certificate_file:
    ensure  => file,
    owner   => $service_name,
    group   => 'root',
    mode    => '0644',
    content => $server_certificate,
    require => File["${config_directory}/certs"],
    notify  => Service[$service_name],
  }
  file { $server_key_file:
    ensure  => file,
    owner   => $service_name,
    group   => 'root',
    mode    => '0600',
    content => $server_private_key,
    require => File["${config_directory}/private"],
    notify  => Service[$service_name],
  }

  $api_clients = $client_identities.map |$identity| {
    {
      id        => $identity['id'],
      key_lines => split($identity['key'], "\n"),
    }
  }

  file { $config_file:
    ensure  => present,
    owner   => $service_name,
    group   => 'root',
    mode    => '0600',
    content => epp('profiles/cacert_selfservice_api/config.yaml.epp', {
      server_certificate             => $server_certificate_file,
      server_key                     => $server_key_file,
      listen_address                 => $listen_address,
      db_username                    => $db_username,
      db_password                    => $db_password,
      db_name                        => $db_name,
      notification_sender            => $notification_sender_address,
      notification_recipient_address => $notification_recipient_address,
      notification_recipient_name    => $notification_recipient_name,
      mail_host                      => $mail_host,
      mail_port                      => $mail_port,
      clients                        => $api_clients,
      log_directory                  => $log_directory,
    }),
    require => Package[$service_name],
    notify  => Service[$service_name],
  }

  service { $service_name:
    ensure  => running,
    enable  => true,
    require => Package[$service_name],
  }
}
