# Class: profiles::jenkins
# ========================
#
# This class takes care if setting up a Jenkins CI server.
#
# Examples
# --------
#
# @example
#   class roles::myhost {
#     include profiles::jenkins
#   }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2019 Jan Dittberner
#
class profiles::jenkins (
) {
  apt::key { 'jenkins':
    id      => '150FDE3F7787E7D11EF4E12A9B7D32F2D50582E6',
    source  => 'https://pkg.jenkins.io/debian/jenkins.io.key',
    options => 'http-proxy=http://proxyout:3128',
  }
  apt::source { 'jenkins':
    location => 'https://pkg.jenkins.io/debian',
    release  => '',
    repos    => 'binary/',
  } ->
  package { 'jenkins':
    ensure  => latest,
  } ->
  service { 'jenkins':
    ensure  => running,
    enable  => true,
  }
}
