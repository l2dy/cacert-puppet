# Class: profiles::icinga2_agent
# ==============================
#
# This class installs and configures an Icinga2 agent.
#
# Parameters
# ----------
#
# @param pki_ticket         Ticket for getting a signed certificate
#                           from the master
#
# @param master_host        Hostname of the master
#
# @param master_certificate TLS certificate of the master
#
# Examples
# --------
#
# @example
#   class roles::myhost {
#     include profiles::icinga2_agent
#   }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2019 Jan Dittberner
class profiles::icinga2_agent (
  String $pki_ticket,
  String $master_host,
  String $master_certificate,
) {
  include 'profiles::icinga2_common'

  $icinga_master_cert = '/var/lib/icinga2/certs/trusted-cert.crt'

  file { $icinga_master_cert:
    ensure  => file,
    content => $master_certificate,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0644',
    require => File['/var/lib/icinga2/certs'],
  }

  class { '::icinga2':
    confd       => false,
    manage_repo => false,
    features    => ['mainlog'],
  }

  class { '::icinga2::feature::api':
    pki             => 'icinga2',
    ca_host         => $master_host,
    ticket_id       => $pki_ticket,
    accept_config   => true,
    accept_commands => true,
    endpoints       => {
      'NodeName'   => {},
      $master_host => {
        host => $master_host,
      },
    },
    zones           => {
      'ZoneName'   => {
        'endpoints' => ['NodeName'],
        'parent'    => $master_host,
      },
      $master_host => {
        'endpoints' => [$master_host],
      },
    },
    require         => File[$icinga_master_cert],
  }

  icinga2::object::zone { 'global-templates':
    global => true,
  }

  @@icinga2::object::endpoint { $::fqdn:
    ensure        => present,
  }

  @@icinga2::object::zone { $::fqdn:
    ensure    => present,
    endpoints => [$::fqdn],
    parent    => $master_host,
  }

  package { 'monitoring-plugins-basic':
    ensure => latest,
  }
}
