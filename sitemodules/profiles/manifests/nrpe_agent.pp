# Class: profiles::nrpe_agent
# ===========================
#
# This class installs and configures the nrpe agent for remote monitoring.
#
# Parameters
# ----------
#
# @param allowed_hosts list of allowed host IP addresses
#
# Examples
# --------
#
# @example
#   class roles::myhost {
#     include profiles::nrpe_agent
#   }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2018 Jan Dittberner
class profiles::nrpe_agent (
   Array[String] $allowed_hosts,
) {
  package { 'nagios-nrpe-server':
    ensure => latest,
  }

  package { 'monitoring-plugins-basic':
    ensure => latest,
  }

  service { 'nagios-nrpe-server':
    ensure => running,
    enable => true,
  }

  file { '/etc/nagios/nrpe_local.cfg':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => epp(
      'profiles/nrpe_agent/nrpe_local.cfg.epp',
      {
        'allowed_hosts' => $allowed_hosts
      }
    ),
    require => Package['nagios-nrpe-server'],
    notify  => Service['nagios-nrpe-server'],
  }

  file { '/etc/nagios/nrpe.d':
    ensure  => directory,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    require => Package['nagios-nrpe-server'],
  }

  file { '/etc/nagios/nrpe.d/apt.cfg':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules//profiles/nrpe_agent/apt.cfg',
    require => [File['/etc/nagios/nrpe.d']],
    notify  => Service['nagios-nrpe-server'],
  }
}
