# Class: profiles::debarchive
# ===========================
#
# This class defines a Debian package archive setup.
#
# Parameters
# ----------
#
# @param notification_email_address email address that will receive reports
#                                   from mini-dinstall
#
# @param release_signing_key        data of a GPG key that is used for
#                                   release file signing
#
# @param release_signing_keygrip    GPG keygrip of the release signing key
#
# @param release_signing_keyid      GPG key id of the release signing key
#
# @param uploaders                  a list of users that are allowed to dput
#                                   files to the Debian archive
#
# Examples
# --------
#
# @example
#   class 'roles::myhost' {
#     include profiles::debarchive
#   }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2019 Jan Dittberner
#
class profiles::debarchive (
  String $notification_email_address,
  String $release_signing_key,
  String $release_signing_keygrip,
  String $release_signing_keyid,
  Array[String] $uploaders = [],
) {
  $debarchive_home = '/srv/debarchive'
  $gpg_home = "${debarchive_home}/.gnupg"
  $package_dir = "${debarchive_home}/packages"
  $trusted_keyring = "${debarchive_home}/cacert-keyring.gpg"
  $archive_public_key = "${package_dir}/cacert-debian-archive-2019.gpg"
  $release_signing_private_key_file = "${gpg_home}/private-keys-v1.d/${release_signing_keygrip}.key"

  $upload_chroot = '/srv/upload'
  $incoming_dir = "${upload_chroot}/incoming"
  $inoticoming_service = '/etc/systemd/system/debarchive-inoticoming.service'

  include profiles::base
  include profiles::apache_common
  include profiles::systemd_reload

  package{ ['rssh', 'reprepro', 'inoticoming']:
    ensure => latest,
  } ->
  file { 'ensure that suid bit on rssh_chroot_helper is set':
    path   => '/usr/lib/rssh/rssh_chroot_helper',
    ensure => present,
    owner  => 'root',
    group  => 'root',
    mode   => '4755',
  }
  exec { 'add rssh to list of valid shells':
    command => '/usr/sbin/add-shell /usr/bin/rssh',
    unless  => '/bin/grep -q rss /etc/shells',
    require => Package['rssh'],
  }

  # setup user, groups and directories
  group { 'debarchive':
    ensure => absent,
  }
  user { 'debarchive':
    ensure         => present,
    comment        => 'CAcert debian archive user',
    system         => true,
    gid            => 'nogroup',
    home           => $debarchive_home,
    shell          => '/usr/bin/rssh',
    purge_ssh_keys => true,
    require        => Package['rssh'],
  }
  file { $debarchive_home:
    ensure => directory,
    owner  => 'debarchive',
    group  => 'nogroup',
    mode   => '0711',
  }
  file { $upload_chroot:
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }
  file { $incoming_dir:
    ensure => directory,
    owner  => 'debarchive',
    group  => 'nogroup',
    mode   => '0700',
  }
  exec { "/bin/bash /usr/share/doc/rssh/examples/mkchroot.sh ${upload_chroot}":
    creates => "${upload_chroot}/usr/bin/rssh",
    require => [Package['rssh'], File[$upload_chroot]],
  } ~>
  exec { "/bin/sed -n -i '/^root:/p; /^debarchive:/p' ${upload_chroot}/etc/passwd":
    refreshonly => true,
  }

  $rssh_conf = '/etc/rssh.conf'

  concat { $rssh_conf:
    ensure => present,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  }

  concat::fragment { 'rssh-global':
    target => $rssh_conf,
    order  => '01',
    source => 'puppet:///modules/profiles/debarchive/rssh.global.conf',
  }

  concat::fragment { 'rssh-debarchive':
    target  => $rssh_conf,
    order   => '10',
    content => "user = \"debarchive:022:000110:${upload_chroot}\"\n",
  }

  # setup ssh keys
  $uploaders.each |String $username| {
    $ssh_keys = $::profiles::base::users[$username]['ssh_keys']
    $ssh_keys.each |Hash[String, Data] $keydata| {
      $keyname = $keydata['name']
      ssh_authorized_key { "debarchive-${username}-${keyname}":
        ensure  => present,
        user    => 'debarchive',
        type    => $keydata['type'],
        key     => $keydata['key'],
        require => User['debarchive'],
      }
    }
  }

  file { $trusted_keyring:
    ensure => file,
    owner  => 'debarchive',
    group  => 'nogroup',
    mode   => '0600',
    source => 'puppet:///modules/profiles/debarchive/cacert-keyring.gpg',
  }

  # setup GPG home for signing
  file { [$gpg_home, "${gpg_home}/private-keys-v1.d", "${debarchive_home}/log"]:
    ensure => directory,
    owner  => 'debarchive',
    group  => 'nogroup',
    mode   => '0700',
  }
  file { $release_signing_private_key_file:
    ensure  => file,
    owner   => 'debarchive',
    group   => 'nogroup',
    mode    => '0600',
    content => $release_signing_key,
  }
  file { "${gpg_home}/gpg-agent.conf":
    ensure  => file,
    owner   => 'debarchive',
    group   => 'nogroup',
    mode    => '0600',
    content => "log-file ${debarchive_home}/log/gpg-agent.log",
  }
  file { "${gpg_home}/gpg.conf":
    ensure  => file,
    owner   => 'debarchive',
    group   => 'nogroup',
    mode    => '0600',
    content => "keyring ${trusted_keyring}\n",
  }
  file { "${gpg_home}/pubring.kbx":
    ensure => file,
    owner  => 'debarchive',
    group  => 'nogroup',
    mode   => '0600',
    source => 'puppet:///modules/profiles/debarchive/gpg_pubring.kbx',
  }
  file { "${gpg_home}/trustdb.gpg":
    ensure => file,
    owner  => 'debarchive',
    group  => 'nogroup',
    mode   => '0600',
    source => 'puppet:///modules/profiles/debarchive/gpg_trustdb.gpg',
  }
  exec { "export archive signing key":
    command => "/usr/bin/gpg --export --export-options export-minimal \"${release_signing_keyid}\" > ${archive_public_key}",
    creates => $archive_public_key,
    user    => 'debarchive',
    require => [
      File[$package_dir],
      File["${gpg_home}/gpg.conf"],
      File["${gpg_home}/gpg-agent.conf"],
      File[$release_signing_private_key_file],
      File["${gpg_home}/pubring.kbx"],
      File["${gpg_home}/trustdb.gpg"],
    ],
  }

  # setup reprepro
  file { $package_dir:
    ensure => directory,
    owner  => 'debarchive',
    group  => 'nogroup',
    mode   => '0755',
  }
  file { "${package_dir}/conf":
    ensure => directory,
    owner  => 'debarchive',
    group  => 'nogroup',
    mode   => '0700',
  }

  concat { "${package_dir}/conf/distributions":
    ensure  => present,
    owner   => 'debarchive',
    group   => 'nogroup',
    mode    => '0600',
  }

  concat::fragment { 'stretch-distribution':
    target  => "${package_dir}/conf/distributions",
    content => join([
      'Origin: CAcert Infrastructure Team',
      'Codename: stretch-cacert',
      'Architectures: amd64 source',
      'Components: main',
      'SignWith: yes',
      'DebIndices: Packages Release . .gz .xz',
      'Uploaders: uploaders',
      "Log: ${debarchive_home}/log/stretch-cacert-updates.log",
      '',
      ''], "\n"),
  }

  concat::fragment { 'buster-distribution':
    target  => "${package_dir}/conf/distributions",
    content => join([
      'Origin: CAcert Infrastructure Team',
      'Codename: buster-cacert',
      'Architectures: amd64 source',
      'Components: main',
      'SignWith: yes',
      'DebIndices: Packages Release . .gz .xz',
      'Uploaders: uploaders',
      "Log: ${debarchive_home}/log/buster-cacert-updates.log",
      '',
      ''], "\n"),
  }

  file { "${package_dir}/conf/incoming":
    ensure => file,
    owner  => 'debarchive',
    group  => 'nogroup',
    mode   => '0600',
    source => 'puppet:///modules/profiles/debarchive/reprepro_conf_incoming',
  }

  file { "${package_dir}/conf/uploaders":
    ensure  => file,
    owner   => 'debarchive',
    group   => 'nogroup',
    mode    => '0600',
    content => "allow * by any key\n",
  }

  file { $inoticoming_service:
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/profiles/debarchive/inoticoming.service',
    require => [
      Package['inoticoming'],
      Concat["${package_dir}/conf/distributions"],
      File["${package_dir}/conf/incoming"],
      File["${package_dir}/conf/uploaders"],
      File[$trusted_keyring],
      User['debarchive'],
    ],
    notify  => Exec['reload systemd configuration'],
  }

  service { 'debarchive-inoticoming':
    ensure  => running,
    enable  => true,
    require => File[$inoticoming_service],
  }

  apache::vhost { 'webstatic.infra.cacert.org':
    port              => 80,
    docroot           => $package_dir,
    manage_docroot    => false,
    access_log        => true,
    access_log_format => 'combined',
    error_log         => true,
    log_level         => 'warn',
    directories       => [
      {
        path    => $package_dir,
        options => ['Indexes', 'FollowSymLinks', 'Multiviews'],
        require => 'all granted',
      },
      {
        path    => "${package_dir}/db",
        require => 'all denied',
      },
      {
        path    => "${package_dir}/conf",
        require => 'all denied',
      },
    ],
  }
}
