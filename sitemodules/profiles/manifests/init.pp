# Class: profiles
# ===============
#
# This is just the empty profiles class. Specific profiles are defined in
# other classes in this module.
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2016 Jan Dittberner
#
class profiles {
}
