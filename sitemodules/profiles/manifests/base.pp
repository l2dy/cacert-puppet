# Class: profiles::base
# =====================
#
# This class defines the base profile that is valid for all puppet managed
# CAcert hosts and should therefore be included in any host role class in the
# roles module.
#
# Parameters
# ----------
#
# @param admins a list of admin users for the node
#
# @param users a hash containing user information
#
# @param rootalias alias that gets emails for root
#
# @param crl_job_enable whether to setup the hourly CRL update job
#
# @param crl_job_services which services to reload after the CRL update
# @param is_external whether the node is outside of CAcert infrastructure
#
# Examples
# --------
#
# @example
#   class roles::myhost {
#     include profiles::base
#   }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2016-2019 Jan Dittberner
#
class profiles::base (
  Array[String] $admins           = [],
  Hash[String, Data] $users       = {},
  String $rootalias               = "${trusted['certname']}-admin@cacert.org",
  Boolean $crl_job_enable         = false,
  Array[String] $crl_job_services = [],
  Boolean $is_external            = false,
) {
  # ensure admin users for this container
  $admins.each |String $username| {
    $user = $users[$username]
    $osusername = $user['username']
    group { $user['username']:
      ensure => present,
    } ->
    user { $osusername:
      ensure         => present,
      comment        => $user['fullname'],
      gid            => $osusername,
      groups         => ['sudo', 'adm'],
      password       => $user['password'],
      uid            => $user['uid'],
      home           => "/home/${osusername}",
      shell          => $user['shell'],
      purge_ssh_keys => true,
      managehome     => true,
    }
    $user['ssh_keys'].each |Hash[String, Data] $keydata| {
      $keyname = $keydata['name']
      ssh_authorized_key { "${osusername}@${keyname}":
        ensure  => present,
        user    => $user['username'],
        type    => $keydata['type'],
        key     => $keydata['key'],
        require => User[$osusername],
      }
    }
  }

  user { 'root':
    ensure => present,
    shell  => '/usr/bin/zsh',
  }

  unless $is_external {
    file { '/etc/apt/apt.conf.d/03proxy':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => 'puppet:///modules/profiles/base/apt_proxy.conf',
    }
  }
  file { '/etc/apt/apt.conf.d/10periodic':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/profiles/base/apt_periodic.conf',
  }

  package { 'lsb-release':
    ensure => present,
  }

  package { ['zsh', 'tmux', 'less']:
    ensure => latest,
  }

  Package["zsh"] -> User <| |>

  package { ['aptitude', 'apticron']:
    ensure => purged,
  }

  file { '/etc/zsh/newuser.zshrc.recommended':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => epp('profiles/base/zshrc.epp'),
    require => Package['zsh'],
  }
  file { '/root/.zshrc':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0640',
    content => epp('profiles/base/zshrc.epp',
      { 'prompttemplate' => 'fire' }),
  }

  include apt
  if $is_external {
    apt::key { 'puppetlabs':
      id      => '6F6B15509CF8E59E6E469F327F438280EF8D349F',
      server  => 'pgp.mit.edu',
    }
  } else {
    apt::key { 'puppetlabs':
      id      => '6F6B15509CF8E59E6E469F327F438280EF8D349F',
      server  => 'pgp.mit.edu',
      options => 'http-proxy=http://proxyout:3128/',
    }
  }
  apt::source { "ftp.nl.debian.org-${::lsbdistcodename}":
    location => 'http://ftp.nl.debian.org/debian',
    repos    => 'main',
    release  => $::lsbdistcodename,
  }
  apt::source { "ftp.nl.debian.org-${::lsbdistcodename}-updates":
    location => 'http://ftp.nl.debian.org/debian',
    repos    => 'main',
    release  => "${::lsbdistcodename}-updates",
  }
  apt::source { "security.debian.org-${::lsbdistcodename}-security":
    location => 'http://security.debian.org/debian-security',
    repos    => 'main',
    release  => "${::lsbdistcodename}/updates",
  }
  apt::source { "ftp.nl.debian.org-${::lsbdistcodename}-backports":
    location => 'http://ftp.nl.debian.org/debian',
    repos    => 'main',
    release  => "${::lsbdistcodename}-backports",
  }
  apt::source { "puppet6-${::lsbdistcodename}":
    location => 'http://apt.puppetlabs.com',
    key      => '6F6B15509CF8E59E6E469F327F438280EF8D349F',
    repos    => 'puppet6',
    release  => $::lsbdistcodename,
  }

  file { '/etc/apt/preferences.d/blacklist_systemd-sysv.pref':
    ensure => absent,
  }

  unless $is_external {
    file { '/etc/resolv.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => 'puppet:///modules/profiles/base/resolv.conf',
    }
  }

  file { '/etc/update-motd.d/20-puppetinfo':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/profiles/base/motd-puppet.sh',
  }

  mailalias { 'root':
    ensure    => present,
    recipient => $rootalias,
  }

  if ($crl_job_enable) {
    package { ['ca-certificates', 'ca-cacert']:
      ensure => installed,
    }

    file { '/var/local/ssl':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0755',
    }

    file { '/var/local/ssl/crls':
      ensure  => directory,
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
      require => File['/var/local/ssl'],
    }

    file { '/etc/cron.hourly/update-crls':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
      content => epp(
        'profiles/base/update-crls.epp',
        { 'services' => $crl_job_services }),
      require => [Package['ca-certificates'], Package['ca-cacert'], File['/var/local/ssl/crls']],
    }
  } else {
    file { '/etc/cron.hourly/update-crls':
      ensure => absent,
    }
  }

  if $::facts['virtual'] == 'lxc' {
    package { 'udev':
      ensure => absent,
    }
  }

  package { 'etckeeper':
    ensure => installed,
  }
  file { '/etc/etckeeper/post-puppet.d':
    ensure  => directory,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    require => Package['etckeeper'],
  }
  file { '/etc/etckeeper/post-puppet.d/50uncommitted-changes':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/profiles/base/etckeeper_post_command.sh',
  }

  augeas { 'set_puppet_postrun_command':
    context => '/etc/puppetlabs/puppet/puppet.conf',
    changes => 'set main/postrun_command "etckeeper post-puppet"',
  }
}
