# Class: profiles::static_websites
# ================================
#
# This class takes care of basic Apache http setup. It is meant to be
# included by other profiles.
#
# Examples
# --------
#
# @example
#   class profiles::myprofile {
#     include profiles::icinga2_agent
#   }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2019 Jan Dittberner
class profiles::apache_common (
) {
  class { 'apache':
    default_vhost => false,
  }
}
