# Class: profiles::systemd_reload
# ===============================
#
# systemd daemon reload execution that can be triggerd from other resources by
# notifying Exec['reload systemd configuration'].
#
# This manifest is meant to be included from other manifests.
#
# Examples
# --------
#
# @example
#   include profiles::systemd_reload
#
#   file { 'myfile':
#     source => 'some_source',
#     notify => Exec['reload systemd configuration'],
#   }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2019 Jan Dittberner
class profiles::systemd_reload (
) {
  exec { 'reload systemd configuration':
    command     => '/bin/systemctl daemon-reload',
    refreshonly => true,
  }
}
