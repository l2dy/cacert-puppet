# Class: profiles::nrpe_agent
# ===========================
#
# This class ensures that the nrpe agent is removed.
#
# Examples
# --------
#
# @example
#   class roles::myhost {
#     include profiles::nrpe_agent
#   }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2019 Jan Dittberner
class profiles::purge_nrpe_agent () {
  package { 'nagios-nrpe-server':
    ensure => purged,
  }

  service { 'nagios-nrpe-server':
    ensure => stopped,
    enable => false,
  }

  file { '/etc/nagios':
    ensure => absent,
    force  => true,
  }
}
