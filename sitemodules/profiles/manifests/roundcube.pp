# Class: profiles::roundcube
# ==========================
#
# This class installs and configures the roundcube webmail system.
#
# Parameters
# ----------
#
# des_key          Key to encrypt the the client cookies, must be exactly 24
#                  characters long
#
# email_host       Hostname of the email server
#
# mail_domain      Mail domain used to find matching user names from client
#                  certificate Email SubjectAlternativeName extensions
#
# master_password  IMAP server master password used for client certificate
#                  authentication
#
# Examples
# --------
#
# @example
#   class roles::myhost {
#     include profiles::roundcube
#   }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2019 Jan Dittberner
class profiles::roundcube (
  String $des_key,
  String $email_host = 'email.infra.cacert.org',
  String $mail_domain = 'cacert.org',
  String $master_password,
) {
  include profiles::cacert_debrepo

  package { 'mariadb-server':
    ensure => latest,
  }

  package { ['dbconfig-mysql', 'php-zip', 'php-gd', 'libapache2-mod-php', 'roundcube', 'roundcube-plugins', 'roundcube-mysql', 'roundcube-plugin-clientcert-authentication']:
    ensure => latest,
  }

  file { '/etc/roundcube/config.inc.php':
    ensure  => file,
    owner   => 'root',
    group   => 'www-data',
    mode    => '0640',
    content => epp('profiles/roundcube/config.inc.php.epp', {
      des_key     => $des_key,
      email_host  => $email_host,
      plugins     => ['clientcert_authentication', 'managesieve'],
      skin        => 'larry',
      smtp_port   => 587,
      support_url => 'https://bugs.cacert.org/set_project.php?project_id=18;30',
    }),
    require => [
      Package['roundcube'],
      Package['roundcube-plugins'],
      Package['roundcube-plugin-clientcert-authentication'],
    ],
  }

  file { '/etc/roundcube/plugins/managesieve/config.inc.php':
    ensure  => file,
    owner   => 'root',
    group   => 'www-data',
    mode    => '0640',
    content => epp('profiles/roundcube/managesieve-config.inc.php.epp', {
    }),
    require => Package['roundcube-plugins'],
  }

  file { '/etc/roundcube/plugins/clientcert_authentication/config.inc.php':
    ensure  => file,
    owner   => 'root',
    group   => 'www-data',
    mode    => '0640',
    content => epp('profiles/roundcube/clientcert_authentication-config.inc.php.epp', {
      mail_domain     => $mail_domain,
      master_password => $master_password,
    }),
    require => Package['roundcube-plugin-clientcert-authentication'],
  }
}
