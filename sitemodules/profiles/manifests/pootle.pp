# Class: profiles::pootle
# =======================
#
# This class defines the pootle setup for translations.cacert.org.
#
# Parameters
# ----------
#
# This class has no parameters
#
# Examples
# --------
#
# @example
#   class roles::myhost {
#     include profiles::pootle
#   }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2018 Jan Dittberner
#
class profiles::pootle {
  group { 'pootle':
    ensure => present,
    gid    => 106,
    system => true,
  }
  user { 'pootle':
    ensure  => present,
    comment => 'Pootle daemon',
    system  => true,
    home    => '/var/lib/pootle',
    shell   => '/bin/false',
    require => Group['pootle'],
  }
  group { 'pootle-update':
    ensure => present,
    gid    => 200,
    system => true,
  }
  class { 'sudo':
    config_file_replace => false,
  }
  file { '/usr/local/bin/pootle-update':
    ensure => file,
    source => 'puppet:///modules/profiles/pootle/pootle-update',
    owner  => 'root',
    group  => 'staff',
    mode   => '0755',
  }
  sudo::conf { 'pootle-update':
    content => '%pootle-update ALL = (pootle) NOPASSWD: /usr/local/bin/pootle-update',
  }
}
