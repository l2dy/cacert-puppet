# Class: profiles::cacert_boardvoting
# ===================================
#
# This class defines the cacert_boardvoting profile that configures the CAcert
# board voting system.
#
# Parameters
# ----------
#
# @param base_url                    base URL where the web interface can be
#                                    found
#
# @param cookie_secret               32 bytes of secret key data for cookie
#                                    encryption
#
# @param csrf_key                    32 bytes of secret key data for CSRF
#                                    protection token encryption
#
# @param mail_host                   hostname or IP address of the outgoing
#                                    email server
#
# @param mail_port                   TCP port number of the outgoing email
#                                    server
#
# @param notice_mail_address         email address that should receive notices
#                                    about new motions and motion status
#                                    changes
#
# @param notification_sender_address email address that is used as the sender
#                                    of generated emails
#
# @param server_certificate          PEM encoded X.509 server certificate
#
# @param server_private_key          PEM encoded unencrypted RSA private key
#
# @param vote_notice_mail_address    email address that should receive
#                                    notification when votes on a motion are
#                                    made
#
# Examples
# --------
#
# @example
#   class roles::myhost {
#     include profiles::cacert_boardvoting
#   }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2018-2019 Jan Dittberner
#
class profiles::cacert_boardvoting (
  String $base_url = "https://motion.cacert.org",
  String $cookie_secret,
  String $csrf_key,
  String $mail_host = 'localhost',
  Integer $mail_port = 25,
  String $notice_mail_address = 'cacert-board@lists.cacert.org',
  String $notification_sender_address = 'returns@cacert.org',
  String $server_certificate,
  String $server_private_key,
  String $vote_notice_mail_address = 'cacert-board-votes@lists.cacert.org',
) {
  include profiles::cacert_debrepo
  package { 'cacert-boardvoting':
    ensure  => latest,
    require => Apt::Source['cacert'],
  } ->
  file { '/srv/cacert-boardvoting/config.yaml':
    ensure  => file,
    owner   => 'cacert-boardvoting',
    group   => 'root',
    mode    => '0600',
    content => epp('profiles/cacert_boardvoting/config.yaml.epp', {
      base_url       => $base_url,
      cookie_secret  => $cookie_secret,
      csrf_key       => $csrf_key,
      mail_host      => $mail_host,
      mail_port      => $mail_port,
      motion_address => $notice_mail_address,
      sender_address => $notification_sender_address,
      vote_address   => $vote_notice_mail_address,
      }),
    notify  => Service['cacert-boardvoting'],
  }
  file { '/srv/cacert-boardvoting/data/cacert_class3.pem':
    ensure => file,
    owner  => 'cacert-boardvoting',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/profiles/base/cacert_class3_X0E.crt',
    notify  => Service['cacert-boardvoting'],
  }
  file { '/srv/cacert-boardvoting/data/server.crt':
    ensure  => file,
    owner   => 'cacert-boardvoting',
    group   => 'root',
    mode    => '0644',
    content => $server_certificate,
    notify  => Service['cacert-boardvoting'],
  }
  file { '/srv/cacert-boardvoting/data/server.key':
    ensure  => file,
    owner   => 'cacert-boardvoting',
    group   => 'root',
    mode    => '0600',
    content => $server_private_key,
    notify  => Service['cacert-boardvoting'],
  }
  service { 'cacert-boardvoting':
    ensure  => running,
    enable  => true,
  }
}
