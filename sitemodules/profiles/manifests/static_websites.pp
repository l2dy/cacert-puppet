# Class: profiles::static_websites
# ================================
#
# This class takes care of VirtualHost setup for static websites.
#
# Parameters
# ----------
#
# @param apache_vhosts Apache VirtualHost definitions that will be fed into
#                      apache::vhost resources from the puppetlabs/apache
#                      module
#
# Examples
# --------
#
# @example
#   class roles::myhost {
#     include profiles::icinga2_agent
#   }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2019 Jan Dittberner
class profiles::static_websites (
  Hash[String, Data] $apache_vhosts = {},
) {
  include profiles::apache_common

  create_resources(apache::vhost, $apache_vhosts)
}
