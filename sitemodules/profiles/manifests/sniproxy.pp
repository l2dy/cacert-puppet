# Class: profiles::sniproxy
# =========================
#
# This class takes care if setting up SNIProxy.
#
# Parameters
# ----------
#
# @param https_forwards a list of server names to target ips/ports
#
# Examples
# --------
#
# @example
#   class roles::myhost {
#     include profiles::sniproxy
#   }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2017 Jan Dittberner
#
class profiles::sniproxy (
  Array[String] $https_forwards,
) {
  file { '/etc/apt/preferences.d/sniproxy':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/profiles/sniproxy/sniproxy_apt_preferences',
  }

  package { 'sniproxy':
    ensure => present,
  }

  file { '/etc/default/sniproxy':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/profiles/sniproxy/etc_default_sniproxy',
    require => Package['sniproxy'],
  }

  file { '/etc/sniproxy.conf':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => epp(
      'profiles/sniproxy/sniproxy.conf.epp',
      {'https_forwards' => $https_forwards}
    ),
    require => Package['sniproxy'],
  }

  service { 'sniproxy':
    ensure    => running,
    enable    => true,
    require   => [Package['sniproxy'], File['/etc/default/sniproxy'], File['/etc/sniproxy.conf']],
    subscribe => [File['/etc/default/sniproxy'], File['/etc/sniproxy.conf']],
  }

  file { '/etc/nginx':
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  } ->
  file { '/etc/nginx/nginx.conf':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/profiles/sniproxy/nginx.conf',
  } ->
  package { 'nginx-light':
    ensure => present,
  } ->
  service { 'nginx':
    ensure => running,
    enable => true,
  }
}
