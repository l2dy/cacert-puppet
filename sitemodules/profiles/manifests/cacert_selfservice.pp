# Class: profiles::cacert_selfservice
# ===================================
#
# This class defines the cacert_selfservice profile that configures the CAcert
# community self service system web interface.
#
# Parameters
# ----------
#
# @param base_url            base URL where the web interface can be found
#
# @param cookie_secret       32 bytes of secret key data for cookie encryption
#
# @param csrf_key            32 bytes of secret key data for CSRF protection
#                            token encryption
#
# @param server_certificate  PEM encoded X.509 server certificate
#
# @param server_private_key  PEM encoded unencrypted RSA private key
#
# @param listen_address      Listening socket address
#
# @param admin_emails        Array containing admins with extended permissions
#
# @param api_client_id       API client identifier
#
# @param api_private_key     PEM encoded ECDSA private key for signing API
#                            requests
#
# @param api_endpoint_url    backend API endpoint URL
#
# Examples
# --------
#
# @example
#   class roles::myhost {
#     include profiles::cacert_selfservice
#   }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2019 Jan Dittberner
#
class profiles::cacert_selfservice (
  String $base_url = "https://selfservice.cacert.org",
  String $cookie_secret,
  String $csrf_key,
  String $server_certificate,
  String $server_private_key,
  String $listen_address = ":8443",
  Array[String] $admin_emails,
  String $api_client_id,
  String $api_private_key,
  String $api_endpoint_url = "https://email.infra.cacert.org:9443/",
) {
  include profiles::cacert_debrepo

  $service_name = 'cacert-selfservice'
  $config_directory = "/etc/${service_name}"
  $config_file = "${config_directory}/config.yaml"
  $server_certificate_file = "${config_directory}/certs/server.crt.pem"
  $server_key_file = "${config_directory}/private/server.key.pem"
  $log_directory = "/var/log/${service_name}"

  $api_ca_file = "${config_directory}/certs/api_cas.pem"
  $client_ca_file = "${config_directory}/certs/client_cas.pem"

  package { $service_name:
    ensure  => latest,
    require => Apt::Source['cacert'],
  }

  file { $log_directory:
    ensure  => directory,
    owner   => $service_name,
    group   => 'root',
    mode    => '0750',
    require => Package[$service_name],
  }
  file { "${config_directory}/certs":
    ensure  => directory,
    owner   => $service_name,
    group   => 'root',
    mode    => '0750',
    require => Package[$service_name],
  }
  file { "${config_directory}/private":
    ensure  => directory,
    owner   => $service_name,
    group   => 'root',
    mode    => '0700',
    require => Package[$service_name],
  }
  file { $server_certificate_file:
    ensure  => file,
    owner   => $service_name,
    group   => 'root',
    mode    => '0644',
    content => $server_certificate,
    require => File["${config_directory}/certs"],
    notify  => Service[$service_name],
  }
  file { $server_key_file:
    ensure  => file,
    owner   => $service_name,
    group   => 'root',
    mode    => '0600',
    content => $server_private_key,
    require => File["${config_directory}/private"],
    notify  => Service[$service_name],
  }
  concat { $client_ca_file:
    ensure  => present,
    owner   => $service_name,
    group   => 'root',
    mode    => '0640',
    require => File["${config_directory}/certs"],
    notify  => Service[$service_name],
  }
  concat::fragment { 'cacert-class3-client-ca':
    tag    => 'cacert-class3-client-ca',
    order  => 10,
    target => $client_ca_file,
    source => 'puppet:///modules/profiles/base/cacert_class3_X0E.crt',
  }
  concat::fragment { 'cacert-class1-client-ca':
    tag    => 'cacert-class1-client-ca',
    order  => 20,
    target => $client_ca_file,
    source => 'puppet:///modules/profiles/base/cacert_class1_X0F.crt',
  }

  file { $api_ca_file:
    ensure  => file,
    owner   => $service_name,
    group   => 'root',
    mode    => '0640',
    source => 'puppet:///modules/profiles/base/cacert_class3_X0E.crt',
    require => File["${config_directory}/certs"],
    notify  => Service[$service_name],
  }

  file { $config_file:
    ensure  => present,
    owner   => $service_name,
    group   => 'root',
    mode    => '0600',
    content => epp('profiles/cacert_selfservice/config.yaml.epp', {
      base_url                => $base_url,
      cookie_secret           => $cookie_secret,
      csrf_key                => $csrf_key,
      server_certificate      => $server_certificate_file,
      server_key              => $server_key_file,
      client_cas              => $client_ca_file,
      listen_address          => $listen_address,
      admin_emails            => $admin_emails,
      api_cas                 => $api_ca_file,
      api_client_id           => $api_client_id,
      api_signature_key_lines => split($api_private_key, "\n"),
      api_endpoint_url        => $api_endpoint_url,
      log_directory           => $log_directory,
    }),
    require => Package[$service_name],
    notify  => Service[$service_name],
  }

  service { $service_name:
    ensure  => running,
    enable  => true,
    require => Package[$service_name],
  }
}
