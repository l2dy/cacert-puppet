# Class: profiles::icinga2_common
# ===============================
#
# Common configuration code for Icinga2 agent and master setups.
#
# This manifest is meant to be included from other manifests.
#
# Examples
# --------
#
# @example
#   include profiles::icinga2_common
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2019 Jan Dittberner
class profiles::icinga2_common (
) {
  include profiles::icinga2_certificates
 
  if $::lsbdistcodename == 'stretch' {
    apt::pin { 'icinga2_backports':
      packages => [
        'icinga2',
        'icinga2-bin',
        'icinga2-common',
        'icinga2-doc',
        'icinga2-ido-pgsql',
        'libicinga2',
        'icingaweb2',
        'icingaweb2-common',
      ],
      priority => 500,
      release  => 'stretch-backports',
    }
    Apt::Pin['icinga2_backports'] -> Class['Apt::Update'] -> Package <| tag == 'icinga2' or tag == 'icinga2-ido-pgsql' |>
  }

  package { 'nagios-plugins-contrib':
    ensure => latest,
  }
}
