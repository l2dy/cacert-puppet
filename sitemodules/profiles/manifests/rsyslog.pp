# Class: profiles::rsyslog
# ========================
#
# This class installs and configures rsyslog
#
# Parameters
# ----------
#
# @param enable_klog whether to enable kernel logging
#
# Examples
# --------
#
# @example
#   class roles::myhost {
#     include profiles::rsyslog
#   }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2017-2018 Jan Dittberner
class profiles::rsyslog (
  Boolean $enable_klog = false,
) {
  package { 'rsyslog':
    ensure => present,
  } ->
  file { '/etc/rsyslog.conf':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => epp(
      'profiles/rsyslog/rsyslog.conf.epp',
      {'enable_klog' => $enable_klog}
    ),
  } ->
  service { 'rsyslog':
    ensure => running,
    enable => true,
  }
}
