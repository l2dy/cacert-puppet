# Class: profiles::icinga2_common
# ===============================
#
# This profile puts certificate in Icinga2 hosts. This can be used to put
# client certificates onto Icinga2 instances that should check mutually
# authenticated TLS connections.
#
# This manifest is meant to be included from other manifests.
#
# Parameters
# ----------
#
# @param certificates  List of Hashes with the keys "name", "key" and
#                      "certificate" that defines a list of certificates
#
# Examples
# --------
#
# @example
#   include profiles::icinga2_certificates
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2019 Jan Dittberner
class profiles::icinga2_certificates (
  Array[Hash[String, String]] $certificates = []
) {
  if $certificates.length > 0 {
    file { ['/etc/icinga2/ssl', '/etc/icinga2/ssl/certs', '/etc/icinga2/ssl/keys']:
      ensure  => directory,
      owner   => 'nagios',
      group   => 'nagios',
      mode    => '0700',
      require => Package['icinga2'],
    }
  }
  $certificates.each |$certificate| {
    if 'name' in $certificate and 'certificate' in $certificate {
      file { "/etc/icinga2/ssl/certs/${certificate[name]}.crt.pem":
        ensure  => file,
        owner   => 'nagios',
        group   => 'nagios',
        mode    =>'0600',
        content => $certificate['certificate'],
      }
      if 'key' in $certificate {
        file { "/etc/icinga2/ssl/keys/${certificate[name]}.key.pem":
          ensure  => file,
          owner   => 'nagios',
          group   => 'nagios',
          mode    =>'0600',
          content => $certificate['key'],
        }
      }
    } else {
      $fields = join(keys($certificate), '\', \'')
      notify { 'missing fields in certificate hash':
        message => "Each certificate block needs a 'name', 'certificate' and an optional 'key': found '${fields}'"
      }
    }
  }
}
