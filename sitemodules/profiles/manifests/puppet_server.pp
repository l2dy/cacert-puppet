# Class: profiles::puppet_server
# ==============================
#
# This class takes care of resources on the puppet server
#
# Parameters
# ----------
#
# @param git_pull_ssh_passphrase passphrase to use for the ssh key to pull
#                                new code from the control repository
# @param git_pull_directory      directory where the puppet control repository
#                                is checked out
# @param git_pull_tokens         list of tokens that are valid to trigger the
#                                git pull hook
#
# Examples
# --------
#
# @example
#   class roles::myhost {
#     include profiles::puppet_server
#   }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2018 Jan Dittberner
class profiles::puppet_server (
  String $git_pull_ssh_passphrase,
  String $git_pull_directory = '/etc/puppetlabs/code/environments/production',
  Array[String] $git_pull_tokens,
) {
  package { 'sshpass':
    ensure => installed,
  }

  package { 'git':
    ensure => installed,
  }

  file { '/usr/local/sbin/git-pull-hook':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0750',
    source  => 'puppet:///modules/profiles/puppet_server/git-pull-hook',
    require => [Package['sshpass'], Package['git']],
  }

  file { '/etc/init.d/git-pull-hook':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/profiles/puppet_server/git-pull-hook.init.sh'
  }

  file { '/etc/git-pull-hook.ini':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0400',
    content => epp(
      'profiles/puppet_server/git-pull-hook.ini.epp',
      {
        'ssh_passphrase' => $git_pull_ssh_passphrase,
        'tokens'         => $git_pull_tokens,
        'git_directory'  => $git_pull_directory,
      }
    )
  }

  service { 'git-pull-hook':
    ensure    => running,
    enable    => true,
    subscribe => [File['/etc/git-pull-hook.ini'], File['/usr/local/sbin/git-pull-hook']],
    require   => [
      File['/etc/init.d/git-pull-hook'], File['/usr/local/sbin/git-pull-hook'],
      File['/etc/git-pull-hook.ini'],
    ],
  }
}