# roles

#### Table of Contents

1. [Description](#description)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Reference](#reference)

## Description

This module defines the CAcert specific host roles. Each role describes one
functionality. Hosts should have one role only.

## Usage

Usage is very specific to the particular profile classes. See the class
documentation for the profiles you want to use.

## Reference

### Classes

#### Public classes

*[`roles::puppetmaster`](#roles-puppetmaster): Defines the puppetmaster role
*[`roles::svnserver`](#roles-svnserver): Defines the subversion server role

## Limitations

This module is designed to be used on CAcert infrastructure only. It is not
designed for reuse anywhere else. The CAcert infrastructure is described at https://infradocs.cacert.org/.
