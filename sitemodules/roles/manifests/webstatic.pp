# Class: roles::webstatic
# =======================
#
# This class defines the webstatic role for the static website server. You
# should assign this class using hiera or via an ENC.
#
# Examples
# --------
#
# @example
#   class { 'roles::webstatic': }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2018-2019 Jan Dittberner
#
class roles::webstatic {
  include profiles::base
  include profiles::rsyslog
  include profiles::purge_nrpe_agent
  include profiles::icinga2_agent
  include profiles::static_websites
  include profiles::debarchive
}
