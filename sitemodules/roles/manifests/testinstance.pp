# Class: roles::testinstance
# ==========================
#
# This class defines the testinstrance role for servers providing test
# instances of the CAcert software. You should assign this class using hiera or
# via an ENC.
#
# Examples
# --------
#
# @example
#   class { 'roles::testinstance': }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2018-2019 Jan Dittberner
#
class roles::testinstance {
  include profiles::base
  include profiles::icinga2_agent
}
