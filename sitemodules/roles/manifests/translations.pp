# Class: roles::translations
# ==========================
#
# This class defines the translations role for the Pootle translation server.
# You should assign this class using hiera or via an ENC.
#
# Examples
# --------
#
# @example
#   class { 'roles::translations': }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2018-2019 Jan Dittberner
#
class roles::translations {
  include profiles::base
  include profiles::rsyslog
  include profiles::purge_nrpe_agent
  include profiles::icinga2_agent
  include profiles::pootle
}
