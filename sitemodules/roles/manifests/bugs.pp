# Class: roles::bugs
# ==================
#
# This class defines the bugs role for the mantis bugtracker. You should assign
# this class using hiera or via an ENC.
#
# Examples
# --------
#
# @example
#   class { 'roles::bugs': }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2018-2019 Jan Dittberner
#
class roles::bugs {
  include profiles::base
  include profiles::rsyslog
  include profiles::purge_nrpe_agent
  include profiles::icinga2_agent
}
