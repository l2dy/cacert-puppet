# Class: roles::email
# ===================
#
# This class defines the email role for the email server. You should assign
# this class using hiera or via an ENC.
#
# Examples
# --------
#
# @example
#   class { 'roles::email': }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2019 Jan Dittberner
#
class roles::email {
  include profiles::base
  include profiles::rsyslog
  include profiles::icinga2_agent
  include profiles::cacert_selfservice_api
}
