# Class: roles::motion
# ====================
#
# This class defines the motion role for the CAcert board motion system.
#
# Examples
# --------
#
# @example
#   class { 'roles::motion': }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2017-2019 Jan Dittberner
#
class roles::motion {
  include profiles::base
  include profiles::rsyslog
  include profiles::purge_nrpe_agent
  include profiles::icinga2_agent
  include profiles::cacert_boardvoting
}
