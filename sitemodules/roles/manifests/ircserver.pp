# Class: roles::ircserver
# ====================
#
# This class defines the ircserver role for the CAcert board ircserver system.
#
# Examples
# --------
#
# @example
#   class { 'roles::ircserver': }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2018-2019 Jan Dittberner
#
class roles::ircserver {
  include profiles::base
  include profiles::rsyslog
  include profiles::purge_nrpe_agent
  include profiles::icinga2_agent
}
