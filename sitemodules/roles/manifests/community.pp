# Class: roles::community
# =======================
#
# This class defines the community role for the community server. You should
# assign this class using hiera or via an ENC.
#
# Examples
# --------
#
# @example
#   class { 'roles::community': }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2019 Jan Dittberner
#
class roles::community {
  include profiles::base
  include profiles::rsyslog
  include profiles::icinga2_agent
  include profiles::roundcube
  include profiles::cacert_selfservice
}
