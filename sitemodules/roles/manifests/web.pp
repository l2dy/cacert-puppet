# Class: roles::web
# =================
#
# This class defines the web role for the incoming web reverse proxy. You
# should assign this class using hiera or via an ENC.
#
# Examples
# --------
#
# @example
#   class { 'roles::web': }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2018-2019 Jan Dittberner
#
class roles::web {
  include profiles::base
  include profiles::rsyslog
  include profiles::purge_nrpe_agent
  include profiles::icinga2_agent
}
