# Class: roles::issue
# ===================
#
# This class defines the issue role for the OTRS issue tracker. You should
# assign this class using hiera or via an ENC.
#
# Examples
# --------
#
# @example
#   class { 'roles::issue': }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2018-2019 Jan Dittberner
#
class roles::issue {
  include profiles::base
  include profiles::rsyslog
  include profiles::purge_nrpe_agent
  include profiles::icinga2_agent
}
