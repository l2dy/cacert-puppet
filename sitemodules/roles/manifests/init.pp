# Class: roles
# ===========================
#
# This is just the empty roles class. Specific roles are defined in other
# classes in this module.
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2016 Jan Dittberner
#
class roles {
}
