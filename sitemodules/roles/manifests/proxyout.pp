# Class: roles::proxyout
# =====================
#
# This class defines the inproxy role for incoming proxy servers. You should
# assign this class using hiera or via an ENC.
#
# Examples
# --------
#
# @example
#   class { 'roles::proxyout': }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2017-2019 Jan Dittberner
#
class roles::proxyout {
  include profiles::base
  include profiles::squid
  include profiles::rsyslog
  include profiles::purge_nrpe_agent
  include profiles::icinga2_agent
}
