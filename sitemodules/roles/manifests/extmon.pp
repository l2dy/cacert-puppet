# Class: roles::extmon
# ====================
#
# This class defines the extmon role for an external monitoring host. You
# should assign this class using hiera or via an ENC.
#
# Examples
# --------
#
# @example
#   class { 'roles::extmon': }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2019 Jan Dittberner
#
class roles::extmon {
  include profiles::base
  include profiles::icinga2_agent
}

