# Class: roles::proxyin
# =====================
#
# This class defines the inproxy role for incoming proxy servers. You should
# assign this class using hiera or via an ENC.
#
# Examples
# --------
#
# @example
#   class { 'roles::proxyin': }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2017-2019 Jan Dittberner
#
class roles::proxyin {
  include profiles::base
  include profiles::rsyslog
  include profiles::sniproxy
  include profiles::purge_nrpe_agent
  include profiles::icinga2_agent
}
