# Class: roles::svnserver
# =======================
#
# This class defines the svnserver role for Subversion servers. You should
# assign this class using hiera or via an ENC.
#
# Examples
# --------
#
# @example
#   class { 'roles::svnserver': }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2016-2019 Jan Dittberner
#
class roles::svnserver {
  include profiles::base
  include profiles::rsyslog
  include profiles::purge_nrpe_agent
  include profiles::icinga2_agent
}
