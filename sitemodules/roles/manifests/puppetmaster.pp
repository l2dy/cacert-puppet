# Class: roles::puppetmaster
# ==========================
#
# This class defines the puppetmaster role for the CAcert puppet master. You
# should assign this class using hiera or via an ENC.
#
# Examples
# --------
#
# @example
#   class { 'roles::puppetmaster': }
#
# Authors
# -------
#
# Jan Dittberner <jandd@cacert.org>
#
# Copyright
# ---------
#
# Copyright 2016-2019 Jan Dittberner
#
class roles::puppetmaster {
  include profiles::base
  include profiles::rsyslog
  include profiles::purge_nrpe_agent
  include profiles::icinga2_agent
  include profiles::puppet_server
}
